import React, { useState } from "react";

import Pagination from "./components/Pagination";

const App = () => {
  const [pageNumber, setPageNumber] = useState(1);

  return (
    <Pagination
      pageNumber={pageNumber}
      numberOfPages={10}
      setPageNumber={setPageNumber}
    />
  );
};

export default App;
