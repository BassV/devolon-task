import React from "react";
import PropTypes from "prop-types";
import { nanoid } from "nanoid";

import "./Pagination.scss";

const leftmostButtonLabel = "<<";
const leftButtonLabel = "<";
const rightmostButtonLabel = ">>";
const rightButtonLabel = ">";

const Pagination = ({ pageNumber, numberOfPages, setPageNumber }) => {
  const getPageArray = () => {
    const pageArr = [];

    if (numberOfPages === 1) {
      return [{ id: nanoid(), number: 1 }];
    }

    for (let i = 1; i <= numberOfPages; i++) {
      if (
        i === 1 ||
        i === pageNumber - 1 ||
        i === pageNumber ||
        i === pageNumber + 1 ||
        i === numberOfPages
      ) {
        pageArr.push({ id: nanoid(), number: i });
      } else if (!pageArr[pageArr.length - 1].dots) {
        pageArr.push({ id: nanoid(), dots: true });
      }
    }

    return pageArr;
  };

  const pageArray = getPageArray();

  const handleLeftmostButtonClick = () => {
    setPageNumber(1);
  };

  const handleLeftButtonClick = () => {
    if (pageNumber > 1) {
      setPageNumber(pageNumber - 1);
    }
  };

  const handleRightButtonClick = () => {
    if (pageNumber < numberOfPages) {
      setPageNumber(pageNumber + 1);
    }
  };

  const handleRightmostButtonClick = () => {
    setPageNumber(numberOfPages);
  };

  return (
    <div className="Pagination">
      <div className="Pagination__Wrapper">
        <button onClick={handleLeftmostButtonClick}>
          {leftmostButtonLabel}
        </button>
        <button
          className="Pagination__Wrapper__LeftButton"
          onClick={handleLeftButtonClick}
        >
          {leftButtonLabel}
        </button>

        {pageArray.map((element) =>
          element.number ? (
            <button
              key={element.id}
              className="Pagination__Wrapper__PageButton"
              active={element.number === pageNumber ? 1 : 0}
              onClick={() => setPageNumber(element.number)}
            >
              {element.number}
            </button>
          ) : (
            <span key={element.id} className="Pagination__Wrapper__Dots">
              ...
            </span>
          )
        )}

        <button
          className="Pagination__Wrapper__RightButton"
          onClick={handleRightButtonClick}
        >
          {rightButtonLabel}
        </button>
        <button onClick={handleRightmostButtonClick}>
          {rightmostButtonLabel}
        </button>
      </div>
    </div>
  );
};

Pagination.propTypes = {
  pageNumber: PropTypes.number.isRequired,
  numberOfPages: PropTypes.number.isRequired,
  setPageNumber: PropTypes.func.isRequired,
};

export default Pagination;
